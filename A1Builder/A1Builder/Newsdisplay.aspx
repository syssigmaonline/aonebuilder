﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Newsdisplay.aspx.cs" Inherits="A1Builder.Newsdisplay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">News</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="list-group">
    <asp:Repeater runat="server" ID="rptnews">
        <ItemTemplate>
<div class="list-group-item list-group-item-action flex-column align-items-start ">
    <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1"><asp:Label ID="lblnewshead" runat="server" Text='<%# Eval("Newsdisplay_Title") %>'></asp:Label></h5>
      <small><asp:Label ID="lblcreatets" sortexpression="lblcreatets" runat="server" Text='<%# Eval("Create_Ts","{0:d-MMM-yyyy hh:mm tt}") %>'></asp:Label></small>
    </div>
    <p class="mb-1"><asp:Label ID="lblnewscontent" runat="server" Text='<%# Eval("Newsdisplay_Content") %>'></asp:Label></p>
    
	</div>
</ItemTemplate>
    </asp:Repeater>
  
	

</div>

</asp:Content>
