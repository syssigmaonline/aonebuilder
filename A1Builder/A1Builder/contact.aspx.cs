﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.IO;

namespace A1Builder
{
    public partial class contact : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                SendHTMLMail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SendHTMLMail()
        {
            try
            {
                string Name = string.Empty;
                string Phone = string.Empty;
                string Email = string.Empty;
                string Body = string.Empty;
                string ToMail = "AmitR@Aonebuilding.Com";
                Name = TxtName.Text;
                Email = TxtEmail.Text;
                Phone = TextPhone.Text;
                Body = TextMessage.Text;
                StreamReader reader = new StreamReader(Server.MapPath("~/A1BuildEnqMail.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", Name);
                myString = myString.Replace("$$Phone$$", Phone);
                myString = myString.Replace("$$Email$$", Email);
               
                myString = myString.Replace("$$Body$$", Body);
                
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage("01a1builders@gmail.com", ToMail,"", myString.ToString());
                mail.Subject = "Enquiry For A1 Builder";
                mail.To.Add("info@aonebuilder.com");
                mail.To.Add("PankajS@Aonebuilding.Com");
                mail.To.Add("sesh@syssigma.com");
                mail.To.Add("phani@syssigma.com");

                //pasing the Gmail credentials to send the email

                System.Net.NetworkCredential mailAuthenticaion = new System.Net.NetworkCredential("01a1builders@gmail.com","build@123");
                System.Net.Mail.SmtpClient mailclient = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25); //smtp.gmail.com 587 //relay-hosting.secureserver.net 25
                mailclient.EnableSsl = false;
                mailclient.UseDefaultCredentials = false;
                mailclient.Credentials = mailAuthenticaion;
                mail.IsBodyHtml = true;
                mailclient.Send(mail);

                lblmsg.Text = "Your Enquiry information has been sent successfully";
                lblmsg.ForeColor = System.Drawing.Color.ForestGreen;

                clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            clear();
            lblmsg.Text = "";
        }
        public void clear()
        {
            try
            {
                TxtName.Text = "";
                TxtEmail.Text="";
                TextPhone.Text="";
                TextMessage.Text="";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}