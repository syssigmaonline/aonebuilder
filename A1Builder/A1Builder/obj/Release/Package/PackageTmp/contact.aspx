﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" enableeventvalidation="false" CodeBehind="contact.aspx.cs" Inherits="A1Builder.contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Contact Us</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
    <form class="form-horizontal" method="post" id="contact-form">
      <div class="row">
	  <div class="col-md-7 text-left brandcolor1">
		<h4 class="bold margin-top-25">OFFICE</h4>
          <p class="brandcolor font-size-120">A O N E BUILDING CONTRACTING LLC </p>
          <p class="margin-bottom-10"><i class="fa fa-map-marker font-size-120 margin-right-5 brandcolor"></i> 6th Floor, The H Business Tower 
</p>
          <p class="padding-left-20 margin-bottom-25"> Sheikh Zayed Road, Dubai, UAE</p>
          <p class="brandcolor font-size-110"><strong>Contact Details</strong></p>
          <p><i class="fa fa-phone font-size-120 margin-right-5 brandcolor"></i> <strong>+9714 3866666</strong></p>
          <p><i class="fa fa-mobile font-size-150 margin-right-5 brandcolor"></i> <strong>+9714 3557750</strong></p>
          <p><i class="fa fa-envelope-o margin-right-5 brandcolor"></i> <a href="mailto: info@aonebuilder.com">info@aonebuilder.com</a></p>
        </div>
        <div class="col-md-5">
          <asp:Label ID="lblmsg" runat="server" Font-Size="Medium" Font-Bold="true" Text=""></asp:Label>
          <h5 class=" margin-bottom-25">Please reach out and a business representative will get back to you shortly.</h5>
          <asp:TextBox runat="server" ID="TxtName" name="form_name" placeholder ="Enter Your Name here" class="form-control margin-bottom-20" />
          <asp:TextBox runat="server" ID="TxtEmail" name="form_name" placeholder ="Enter Email Address here" class="form-control margin-bottom-20" />
          <asp:TextBox runat="server" ID="TextPhone" name="form_name" placeholder ="Enter Telephone number here" class="form-control margin-bottom-20" />
          <asp:TextBox runat="server" ID="TextMessage" name="form_name" placeholder ="Enter your message here" rows="5" value="" textmode="MultiLine" class="form-control margin-bottom-20" />

          <%--<input id="fname" name="form_name" type="text" placeholder="Your Name" class="form-control margin-bottom-15" required="">
          <input id="email" name="form_email" type="email" placeholder="E-Mail Address" class="form-control margin-bottom-15" required>
          <input id="Phone" name="form_number" placeholder="Phone Number" class="form-control margin-bottom-15" required maxlength="12" type="number">
          <textarea class="form-control margin-bottom-15" id="message" name="form_message" placeholder="Enter Your Message" rows="3" ></textarea>--%>
            <div class="form-actions text-center margin-top-bottom-20">
            <asp:Button runat="server" ID="btnSubmit" class="btn btn-lg btn-info" Text="Submit" OnClick="btnSubmit_Click"></asp:Button>
            <asp:Button runat="server" ID="btnReset" class="btn btn-lg btn-default margin-left-20" Text="Reset" OnClick="btnReset_Click"></asp:Button>
          </div>
          <%--<div class="form-actions text-right margin-top-bottom-15">
            <button type="reset" class="btn btn-light margin-left-10" name="form_contact" value="Reset">Clear</button>
            <button type="submit" class="btn btn-success min-width-150" name="form_contact" value="Sign Up">Submit</button>
          </div>--%>
        </div>
        
      </div>
    </form>
  </div>
</div>
<div class="container-fluid spl-section padding-top-bottom-0">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1276.0448976144496!2d55.28666112671789!3d25.230187816711037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f42e8ad663999%3A0x42520876c7be8ab1!2sThe+H+Dubai!5e0!3m2!1sen!2sin!4v1515609752698" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</asp:Content>
