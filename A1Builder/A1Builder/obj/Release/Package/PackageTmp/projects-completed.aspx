﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="projects-completed.aspx.cs" Inherits="A1Builder.projects_completed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Completed Projects</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  
  <div class="container">
    <div class="row">
      <div class="col-md-4 text-center"> <a href="#" data-toggle="modal" data-target="#modalAlZorah"><img src="http://www.alzorah.ae/media/4007/dsc_0088-2.jpg" border="0" class="mx-auto max-height-300 img-thumbnail" /></a></div>
       <div class="col-md-8"> <h4>Paving work on Al Zorah</h4>
        <p>Al Zorah is a 25-minute drive from Dubai International Airport and 20 minutes from Sharjah International Airport. The development includes world-class hotels and resorts, residences, commercial spaces, leisure facilities and a 18-hole Golf Course set within a natural preserved environment of mangroves and sandy beaches.</p>
		<strong>A PEACEFUL SANCTUARY OF NATURAL BEAUTY</strong>

		<p>From a gently curving scenic pathway alongside a lagoon, Al Zorah reveals it's unblemished shoreline and natural wonders. The undisturbed ecosystem of the site is home to a huge range of marine life, migratory birds, and plants that are native to the area.</p>
      </div>
    </div>
	  <hr>
  </div>
  
</div>

</asp:Content>
