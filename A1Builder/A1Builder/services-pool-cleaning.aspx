﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="services-pool-cleaning.aspx.cs" Inherits="A1Builder.services_pool_cleaning" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Pool Cleaning</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
        <p>Regular pool cleaning and maintaining chemical content of water to its required level is extremely important. This requires professional team involvement owing to the health and safety issues. We carry out this this job with our specially trained team to ensure that the purity of the water is maintained. For each and every pool that we maintain , we keep a LOG SHEET at site that is maintained by pool cleaning technician including physical status and chemical content of the water. Our Engineer checks the same and carry out the spot test monthly thrice to ensure the records mentioned in the reports are correct. In our LOG SHEET we have provision for clients to record the complaints, if any and the same will be attended to the client’s satisfaction.
</p>
<hr>
    <div class="row">
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/main-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
    </div>
  </div>
</div>

</asp:Content>
