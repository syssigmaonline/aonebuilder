﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using A1Builder.Models;
namespace A1Builder
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblErrorMsg.Text = "";
            if (this.User.Identity.IsAuthenticated)
            {
                Response.Redirect("AdminHome.aspx", false);
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var db = new aonebuilder();
            var login = db.Login_Aonebuild.Where(x => x.Username.Equals(txtUserName.Text.Trim()) && x.Password.Equals(txtPassword.Text.Trim())).Select(x => new
            {
                UserName = x.Username
            }).ToList();
            if (login.Count != 0)
            {
                Session["UserName"] = login[0].UserName;
                FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, true);
                Response.Redirect("AdminHome.aspx", false);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}
