﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="services-hard-landscaping.aspx.cs" Inherits="A1Builder.services_hard_landscaping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Hard Landscaping</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
        <p>Hard landscaping consists of landscape features such as walkways, plazas and patios, which are
constructed with hard raw materials such as timber, concrete and granite. We use a wide variety of
these materials in the creation of numerous beautiful hardscape features. We guarantee our client’s
designs will be implemented with precision and quality construction through the aid of our skilled and
qualified professionals.</p>
<hr>
    <div class="row">
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/l1-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/l3-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/l2-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
    </div>
  </div>
</div>


</asp:Content>
