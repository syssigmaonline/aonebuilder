﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="about-overview.aspx.cs" Inherits="A1Builder.about_overview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Overview</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
    <h4>About Us </h4>
        <p> 
          AOne Building Contracting LLC has been established by Mr.Amit Rupchandani, Mr Pankaj Jani, Mr
Avtar Madan &amp; Mr Pankaj Sutaria. The company is backed by over several decades of cumulative
experience in design, engineering, construction of many Buildings. We are one point contact for all
your construction &amp; landscaping needs.</p>
<hr>
<h4>Vision</h4>
<p>Our vision is to provide unmatched professional services in this sector where integrity, trust and high
quality is of paramount importance.</p>
      </div>
      <div class="col-sm-4"> <img src="images/AOne-poster.jpg" alt="About AOne Builder" class="img-responsive img-thumbnail" /> </div>
    </div>
  </div>
</div>

</asp:Content>
